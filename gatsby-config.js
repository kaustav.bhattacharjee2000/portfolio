module.exports = {
  pathPrefix: `/portfolio`,
  siteMetadata: {
    title: "Kaustav's Portfolio",
    links: {
      resume: 'https://dropnext.gatsbyjs.io/share/-qN1wlrM',
      amici: {
        base: 'https://amici.ai/'
      },
      aimonk: {
        base: 'https://aimonk.com/',
        neuralmarker: 'https://neuralmarker.ai/'
      },
      projects: {
        dropnext: {
          deployed: 'https://dropnext.gatsbyjs.io',
          code: 'https://gitlab.com/kaustav.bhattacharjee2000/DropNext'
        },
        roster: { deployed: 'https://roster-client.herokuapp.com/', code: 'https://github.com/KaustavRaj/roster' },
        save2qr: {
          code: {
            extension: 'https://gitlab.com/kaustav.bhattacharjee2000/save2qr/-/tree/dev',
            app: 'https://gitlab.com/kaustav.bhattacharjee2000/save2qr-app'
          }
        },
        pms: { code: 'https://github.com/KaustavRaj/Property-Management' },
        cakebot: { code: 'https://github.com/KaustavRaj/CakeBot' },
        ic: { code: 'https://github.com/KaustavRaj/Image-Captioning' }
      },
      personal: {
        gmail: 'mailto:kaustav.bhattacharjee2000@gmail.com',
        linkedin: 'https://www.linkedin.com/in/kaustavbhatt/',
        github: 'https://github.com/KaustavRaj',
        gitlab: 'https://gitlab.com/kaustav.bhattacharjee2000'
      }
    },
    skills: [
      { title: 'Frontend', skills: ['react', 'html', 'antd', 'mui', 'css', 'gatsby', 'js'] },
      {
        title: 'Backend',
        skills: ['nodejs', 'mongodb', 'mysql', 'aws', 'express', 'graphql'],
        styles: { height: '3.5rem', width: '3.5rem' }
      },
      {
        title: 'Machine Learning',
        skills: ['tf', 'torch', 'py', 'cv', 'scikit'],
        styles: { height: '3.5rem', width: '3.5rem' }
      },
      { title: 'Miscellaneous', skills: ['qt', 'cpp', 'git', 'expo', 'rn'], styles: { height: '3rem', width: '3rem' } }
    ]
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-styled-components',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`
      }
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'gatsby-starter-default',
        short_name: 'starter',
        start_url: '/',
        background_color: '#f6f1ed',
        theme_color: '#f6f1ed',
        display: 'minimal-ui',
        icon: 'src/images/favicon.png'
      }
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /skills\/.*\.svg/
        }
      }
    }
  ]
};

[![Netlify Status](https://api.netlify.com/api/v1/badges/33bfca67-dcc4-4402-bcf0-1de922f5be68/deploy-status)](https://app.netlify.com/sites/kaustavbhatt/deploys)

<h1 align="center">
  Portfolio website built in Gatsby
</h1>

## 🚀 Quick start

1.  **Install the dependencies.**

    ```sh
    # install the dependencies
    yarn install
    ```

2.  **Run it. Open the source code and start editing!**

    Navigate into your your portfolio's directory and start it up.

    ```sh
    gatsby develop
    ```

    Your site is now running at `http://localhost:8000`!

3.  **Build it.**

    After applying your changes, run this command to deploy it.

    ```sh
    gatsby build
    gatsby serve
    ```

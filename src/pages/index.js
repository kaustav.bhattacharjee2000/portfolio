import React from 'react';
import styled from 'styled-components';
import * as Mixins from '../Mixins';
import * as t from '../Typography';
import Layout, { Content } from '../components/Layout';
import { HireMe, LinkButton } from '../components/Button.js';
import HireMePopup from '../components/HireMePopup.js';
import { media } from '../MediaQueries';
import Colors from '../Colors';
import Img from 'gatsby-image';
import { graphql } from 'gatsby';
import { darken } from 'polished';
import SkillsGrid from '../components/SkillsGrid';
import DropNext from '../images/dropnext.png';
import Roster from '../images/roster.jpg';
import Save2QR from '../images/save2qr.jpg';
import Cakebot from '../images/cakebot.jpeg';
import PMS from '../images/pms.jpg';
import IC from '../images/ic.jpg';

const AboveFold = styled.div`
  ${Mixins.aboveFoldMixin}
  padding: 140px 0 60px 0;
  ${t.H1} {
    color: white;
  }
`;

const Block = styled.div`
  &:nth-child(odd) {
    // border: solid 1px ${darken(0.1, Colors.light)};
    // background-color: ${Colors.light};
  }
`;

const BlockContent = styled(Content)`
  ${Mixins.block}
  padding: 100px 40px;
  ${media.tablet`
    flex-direction: column;
    align-items: baseline;
  `};
  ${media.phone`
    padding: 40px 10px;
  `};
  ${t.P} {
    margin-top: 10px;
  }
  ${t.H2} {
    margin-top: 0;
  }
  img {
    width: 100%;
    height: auto;
  }
`;

const DivWrapper = styled.div`
  padding: 80px 30px;
  ${media.tablet`padding: 50px 0;`}
  &:first-child {
    ${media.tablet`
      margin-bottom: 40px;
  `};
  }
`;

const ItemImage = styled.img`
  max-width: 85%;
  position: relative;
  ${media.tablet`max-width: none;`}
`;

const HomepageWrapper = styled.div`
  ${Mixins.wrapper}
  .who-desc {
    display: block;
    margin: 0 auto 60px auto;
    max-width: 90%;
  }
  ${t.LargeP} {
    margin-bottom: 0;
    color: #abb2bf;
  }
  ${t.H1} {
    margin: 0 0 20px 0;
  }
  .avatar {
    max-width: 200px;
    width: 80%;
    margin: 0 auto 50px auto;
    border-radius: 50%;
    display: block;
    box-shadow: 0 2px 24px 0 rgba(0, 0, 0, 0.1);
    ${media.tablet`max-width: 70%;`}
  }
  .link {
    padding: 0;
    color: ${Colors.orange};
    text-decoration: underlined;
    svg {
      margin-left: 7px;
    }
  }
  .portfolio {
    font-size: 42px;
  }
`;

const WorkWithMe = styled.div`
  padding: 80px;
  width: 73%;
  border-radius: 6px;
  box-shadow: 0 2px 26px 0 rgba(57, 55, 55, 0.08);
  background-color: #ffffff;
  text-align: center;
  position: relative;
  margin: 100px auto -150px auto;
  ${t.LargeP} {
    max-width: 80%;
    margin: 0 auto 28px auto;
  }
  ${media.tablet`
    width: auto;
    padding: 40px;
    margin: 50px 30px -100px 30px;
  `};
`;

const Gap = styled.div`
  width: 100%;
  margin-top: ${props => props.dist || '5rem'};
`;

class Homepage extends React.Component {
  state = {
    openHireMePopup: false
  };

  handleRequestDemoClose = () => {
    this.setState({
      openHireMePopup: false
    });
  };

  openContactPopup = () => {
    this.setState({
      openHireMePopup: true
    });
  };

  render() {
    const { openHireMePopup } = this.state;
    const { data } = this.props;
    const { links, skills } = data.Site.siteMetadata;
    const modifiedName = '{ Kaustav Bhattacharjee }';

    return (
      <HomepageWrapper>
        <Layout theme="white" bigFooter openContactPopup={this.openContactPopup}>
          <AboveFold>
            <Img fluid={data.avatarHomepage.childImageSharp.fluid} alt="Kaustav Bhattacharjee" className="avatar" />
            <t.H1 primary align="center" style={{ color: Colors.purple }}>
              {modifiedName}
            </t.H1>
            <t.LargeP align="center" max45>
              <strong>Hi there !</strong> Welcome to my personal website. I'm an avid fan of developing something when
              it comes to fullstack web dev and machine learning, or a combination of both. Currently pursuing bachelors
              in CS.
            </t.LargeP>
            <Gap dist={'2rem'} />
            <a href={links.resume} target="_blank" rel="noreferrer">
              <HireMe large book>
                Download Resume
              </HireMe>
            </a>
          </AboveFold>

          <Content>
            <t.H2 primary align="center" bold style={{ color: Colors.red }}>
              @ Experience
            </t.H2>

            <div>
              <t.LargeP bold style={{ color: Colors.blue }}>
                Software Engineer
              </t.LargeP>

              <t.P style={{ color: Colors.lightWhite }}>
                <LinkButton
                  primary
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.amici.base}
                  space
                  rel="noreferrer"
                >
                  <strong>Amici</strong>
                </LinkButton>
                - <em>May'21 to present</em>
              </t.P>

              <t.P style={{ color: Colors.lightWhite }}>
                <ul>
                  <li>Working simultaneously with a team of developers on mobileapp, website and backend</li>
                  <li>
                    Converted several REST API endpoints to GraphQL API, and incorporated those changes on website
                  </li>
                  <li>Improved mobile app UI for friends feed page</li>
                </ul>
              </t.P>
            </div>

            <Gap />

            <div>
              <t.LargeP bold style={{ color: Colors.blue }}>
                Associate Machine Learning Engineer
              </t.LargeP>

              <t.P style={{ color: Colors.lightWhite }}>
                <LinkButton
                  primary
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.aimonk.base}
                  space
                  rel="noreferrer"
                >
                  <strong>AIMonk Labs</strong>
                </LinkButton>
                - <em>Apr'20 to Jul'20</em>
              </t.P>

              <t.P style={{ color: Colors.lightWhite }}>
                <ul>
                  <li>
                    Worked as an intern with a team of developers to maintain and improve{' '}
                    <LinkButton
                      primary
                      bold
                      className="link"
                      as="a"
                      target="_blank"
                      href={links.aimonk.neuralmarker}
                      rel="noreferrer"
                    >
                      neuralmarker.ai
                    </LinkButton>
                    , the AI Data Annotation platform of the company.
                  </li>
                  <li>Deployed 10+ computer vision models on AWS using Lambda and SQS.</li>
                  <li>Additionally learnt PyTorch framework and MongoDB Atlas.</li>
                  <li>Performed backend API testing using Postman.</li>
                </ul>
              </t.P>
            </div>
          </Content>

          <Content>
            <DivWrapper>
              <t.H2 primary align="center" bold style={{ color: Colors.red }}>
                @ Skills
              </t.H2>
              <Gap dist="4rem" />
              <SkillsGrid data={skills} />
            </DivWrapper>
          </Content>

          <t.H2 primary align="center" bold className="portfolio" style={{ color: Colors.red }}>
            @ Projects
          </t.H2>

          <Block>
            <BlockContent>
              <DivWrapper>
                <t.H2 bold style={{ color: Colors.blue }}>
                  DropNext
                </t.H2>
                <t.P>
                  A file storage system made for people (like me) who want more control over their online stored files
                  and folders. Comes with basic drive functionalities like uploading files, creating folders, sharing
                  files, deleting them etc. Additionally it will create short link for any number of publicly shared
                  files with the option of adding an expiration date to it (deadline passed no more file viewing). Dark
                  and light theme modes are also included.
                </t.P>
                <LinkButton
                  primary
                  bold
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.projects.dropnext.deployed}
                  rel="noreferrer"
                  space
                >
                  Deployed
                </LinkButton>
                <LinkButton
                  primary
                  bold
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.projects.dropnext.code}
                  rel="noreferrer"
                >
                  Code
                </LinkButton>
              </DivWrapper>
              <DivWrapper>
                <ItemImage src={DropNext} alt="Placeholder title" />
              </DivWrapper>
            </BlockContent>
          </Block>

          <Block>
            <BlockContent>
              <DivWrapper>
                <ItemImage src={Roster} alt="Placeholder title" />
              </DivWrapper>
              <DivWrapper>
                <t.H2 bold style={{ color: Colors.blue }}>
                  Roster
                </t.H2>
                <t.P>
                  Created a Kanban-style collaborative web application for organizing tasks in board. Supports all basic
                  functionalities of Trello, including autocomplete search to add new members. All REST API endpoints
                  are secured with JWT. Used MERN stack, Ant Design and MongoDB Atlas for developing it.
                </t.P>
                <LinkButton
                  primary
                  bold
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.projects.roster.deployed}
                  rel="noreferrer"
                  space
                >
                  Deployed
                </LinkButton>
                <LinkButton
                  primary
                  bold
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.projects.roster.code}
                  rel="noreferrer"
                >
                  Code
                </LinkButton>
              </DivWrapper>
            </BlockContent>
          </Block>

          <Block>
            <BlockContent>
              <DivWrapper>
                <t.H2 bold style={{ color: Colors.blue }}>
                  Save2QR
                </t.H2>
                <t.P>
                  Made a browser extension and a mobile app that transfers multiple browser tabs across devices with the
                  help of QR code. The browser extension also has the functionality to export/import all browser windows
                  with a single click. Previously scanned QR codes can also be saved in the mobile app.
                </t.P>
                <LinkButton
                  primary
                  bold
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.projects.save2qr.code.extension}
                  rel="noreferrer"
                  space
                >
                  Extension
                </LinkButton>
                <LinkButton
                  primary
                  bold
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.projects.save2qr.code.app}
                  rel="noreferrer"
                >
                  App
                </LinkButton>
              </DivWrapper>
              <DivWrapper>
                <ItemImage src={Save2QR} alt="Placeholder title" />
              </DivWrapper>
            </BlockContent>
          </Block>

          <Block>
            <BlockContent>
              <DivWrapper>
                <ItemImage src={PMS} alt="Placeholder title" />
              </DivWrapper>
              <DivWrapper>
                <t.H2 bold style={{ color: Colors.blue }}>
                  Property Management System
                </t.H2>
                <t.P>
                  Made the internal portal of a property management company, where a manager can track the progress of
                  agent in selling/renting properties to the clients. Created the backend and REST APIs using Node.js
                  and MySQL database.
                </t.P>
                <LinkButton
                  primary
                  bold
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.projects.pms.code}
                  rel="noreferrer"
                >
                  Code
                </LinkButton>
              </DivWrapper>
            </BlockContent>
          </Block>

          <Block>
            <BlockContent>
              <DivWrapper>
                <t.H2 bold style={{ color: Colors.blue }}>
                  CakeBot
                </t.H2>
                <t.P>
                  A cake ordering software with a chat interface, through which a customer can place or delete cake
                  order(s), and receive an invoice email. Made using Qt, C++, Python and Gmail API. Contains its own
                  installer for windows 10 OS, made using Qt installer framework.
                </t.P>
                <LinkButton
                  primary
                  bold
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.projects.cakebot.code}
                  rel="noreferrer"
                  space
                >
                  Software
                </LinkButton>
                <LinkButton
                  primary
                  bold
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.projects.cakebot.code}
                  rel="noreferrer"
                >
                  Code
                </LinkButton>
              </DivWrapper>
              <DivWrapper>
                <ItemImage src={Cakebot} alt="Placeholder title" style={{ maxHeight: 500, maxWidth: 330 }} />
              </DivWrapper>
            </BlockContent>
          </Block>

          <Block>
            <BlockContent>
              <DivWrapper>
                <ItemImage src={IC} alt="Placeholder title" />
              </DivWrapper>
              <DivWrapper>
                <t.H2 bold style={{ color: Colors.blue }}>
                  Image Captioning
                </t.H2>
                <t.P>
                  A deep-learning model made using Keras framework and trained on Flickr8k dataset, which can predict a
                  suitable caption for a given image.
                </t.P>
                <LinkButton
                  primary
                  bold
                  className="link"
                  as="a"
                  target="_blank"
                  href={links.projects.ic.code}
                  rel="noreferrer"
                >
                  Code
                </LinkButton>
              </DivWrapper>
            </BlockContent>
          </Block>

          <WorkWithMe>
            <t.H1 style={{ color: Colors.red }}>Get in touch with me</t.H1>
            <t.LargeP style={{ color: Colors.bgGrey, textAlign: 'center' }}>
              Fancy working with me? Contact me for more info!{' '}
            </t.LargeP>
            <HireMe onClick={this.openContactPopup} book default>
              Contact me
            </HireMe>
          </WorkWithMe>
        </Layout>
        <HireMePopup open={openHireMePopup} handleClose={this.handleRequestDemoClose} />
      </HomepageWrapper>
    );
  }
}

export default Homepage;

export const pageQuery = graphql`
  query {
    avatarHomepage: file(relativePath: { eq: "avatar.jpg" }) {
      ...fluidImage
    }
    Site: site {
      siteMetadata {
        links {
          resume
          amici {
            base
          }
          aimonk {
            base
            neuralmarker
          }
          projects {
            cakebot {
              code
            }
            ic {
              code
            }
            pms {
              code
            }
            dropnext {
              code
              deployed
            }
            roster {
              code
              deployed
            }
            save2qr {
              code {
                app
                extension
              }
            }
          }
        }
        skills {
          skills
          title
          styles {
            height
            width
          }
        }
      }
    }
  }
`;

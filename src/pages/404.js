import React from 'react';
import styled from 'styled-components';
import * as Mixins from '../Mixins';
import * as t from '../Typography';
import Layout from '../components/Layout';
import Colors from '../Colors';
import { Button } from '../components/Button.js';
import HireMePopup from '../components/HireMePopup.js';

const AboveFold = styled.div`
  ${Mixins.aboveFoldMixin}
  padding: 157px 0 100px 0;
`;

const NotFoundPageWrapper = styled.div`
  ${Mixins.wrapper}
  .m-b-60 {
    margin-bottom: 60px;
  }
  ${t.H1} {
    margin: 0 0 20px 0;
    color: ${Colors.red};
  }
`;

class NotFoundPage extends React.Component {
  state = {
    openHireMePopup: false
  };

  closeContactPopup = () => {
    this.setState({
      openHireMePopup: false
    });
  };

  openContactPopup = () => {
    this.setState({
      openHireMePopup: true
    });
  };

  render() {
    const { openHireMePopup } = this.state;
    return (
      <NotFoundPageWrapper>
        <Layout theme="white" openContactPopup={this.openContactPopup}>
          <AboveFold>
            <t.H1 green align="center">
              404 Error
            </t.H1>
            <t.H3 green align="center">
              You are on a page yet to be born
            </t.H3>
            <Button to="/">Back to Home</Button>
          </AboveFold>
        </Layout>
        <HireMePopup open={openHireMePopup} handleClose={this.closeContactPopup} />
      </NotFoundPageWrapper>
    );
  }
}

export default NotFoundPage;

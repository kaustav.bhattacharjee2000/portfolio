const Colors = {
  white: '#ffffff',
  light: '#f6f1ed',
  lightGrey: '#e5dada',
  dark: '#b5a397',
  darker: '#6a5750',
  darkest: '#30231d',
  closeIcon: '#969ea4',
  lightBtn: '#61afef',
  lightWhite: '#abb2bf',
  red: 'hsl(355, 65%, 65%)',
  darkRed: 'hsl(355, 43%, 56%)',
  blue: 'hsl(207, 82%, 66%)',
  orange: 'hsl( 29, 54%, 61%)',
  bgGrey: '#282c34',
  darkGrey: '#212329',
  purple: '#c678dd'
};

export default Colors;
